<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

class BFormHelper extends Helper
{
    public $helpers = ['Form'];
	public function control( $name, $options = [] )
	{
		if( !isset( $options['class'] ) ) {
		   $options['class'] = 'form-control';
		}
		return $this->Form->control( $name, $options );
	}
}
