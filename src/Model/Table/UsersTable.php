<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

class UsersTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('UserTypes', [
            'foreignKey' => 'user_type_id',
            'joinType' => 'INNER'
        ]);

        $this->hasOne('UserProfiles',[
            'className' => 'UserProfiles'
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('username')
            ->maxLength('username', 20)
            ->requirePresence('username', 'create')
            ->notEmpty('username');

        $validator
            ->scalar('password')
            ->maxLength('password', 100)
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator->add('password', [
                'compare' => [
                    'rule'   => ['compareWith', 'confirm_password'],
                    'message'=> "Password match failed!!"
                ]
        ]);

        return $validator;
    }

    public function validationPassword(Validator $validator ) { 
        $validator 
        ->add('old_password','custom',[ 'rule'=> function($value, $context)
            { 
              $user = $this->get($context['data']['id']); 
              if ($user) { 
                if ((new DefaultPasswordHasher)->check($value, $user->password)) 
                       { return true; } 
                } return false; 
            }, 
            'message'=>'The old password does not match the current password!', ]) ->notEmpty('old_password'); 
            
            $validator->notEmpty('new_password');
            $validator->notEmpty('confirm_password');

            $validator->add('new_password', [
                'compare' => [
                    'rule'   => ['compareWith', 'confirm_password'],
                    'message'=> "Password match failed!!"
                ]
            ]);


            return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['user_type_id'], 'UserTypes'));

        return $rules;
    }
}
