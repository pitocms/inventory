<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class UserProfile extends Entity
{

    protected $_accessible = [
        'user_id' => true,
        'first_name' => true,
        'last_name' => true,
        'date_of_birth' => true,
        'father_name' => true,
        'mother_name' => true,
        'image' => true,
        'phone_number' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'dagination'=> true
    ];
}
