<?php
namespace App\Model\Entity;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

class User extends Entity
{

    protected $_accessible = [
        'user_type_id' => true,
        'username' => true,
        'password' => true,
        'email' => true,
        'uid' => true,
        'is_email' => true,
        'created' => true,
        'modified' => true,
        'user_type' => true,
        'status'=>true
    ];
    protected $_hidden = [
        'password'
    ];

    protected function _setPassword($password)
    {
        if (strlen($password) > 0) {
          return (new DefaultPasswordHasher)->hash($password);
        }
    }
}
