<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;


class AppController extends Controller
{

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        
        $this->loadComponent( 'Auth', [
            'authenticate' => [
                'Form' => [
                   'fields' => [ 'username' => 'email' ],
                   'contain'   => ['UserProfiles']
                ]
            ],
            'loginRedirect' => [
                'controller' => 'users',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'users',
                'action' => 'login'
            ],
            //'authorize' => array( 'Controller' )
        ]);

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
    }

    public function beforeFilter(Event $event)
    {
        $this->viewBuilder()->setLayout('adminlte');
        $this->Auth->allow(['login','add']);
        $this->set('userAuth',$this->Auth->user());
    }
}
