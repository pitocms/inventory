<?php
namespace App\Controller;
use Cake\Utility\Text;
use App\Controller\AppController;
use Cake\Filesystem\File;

class UserProfilesController extends AppController
{

    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $userProfiles = $this->paginate($this->UserProfiles);

        $this->set(compact('userProfiles'));
    }

    public function view($id = null)
    {
        $userProfile = $this->UserProfiles->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('userProfile', $userProfile);
    }

    public function add()
    {
        $user_id = $this->Auth->user('id');
        
        $query = $this->UserProfiles->find('all', [
            'conditions' => ['user_id' => $user_id]
        ]);
        $row = $query->first();
        $count = $query->count();

        if($count!=0)
        {
            return $this->redirect(['controller'=>'UserProfiles','action' => 'edit']);
        }

        $UserProfile = $this->UserProfiles->newEntity();
        if ($this->request->is('post')) {

            $image_name = $this->request->getData('image.name');
            $image_temp = $this->request->getData('image.tmp_name');
            
            // debug($this->request->getData('image'));
            // exit;

            if(!empty($image_name)){
                $image_name = Text::uuid().$image_name;
                $destination = WWW_ROOT.'img'.DS.'profile_img'.DS.$image_name;

                move_uploaded_file($image_temp, $destination);
                $this->request->data['image'] = 'profile_img/'.$image_name;
            }

            $UserProfile = $this->UserProfiles->patchEntity($UserProfile, $this->request->getData());
            $UserProfile->user_id = $this->Auth->user('id');
            if ($this->UserProfiles->save($UserProfile)) {
                $this->Flash->success(__('The user profile has been saved.'));

                return $this->redirect($this->Auth->logout());
            }
            $this->Flash->error(__('The user profile could not be saved. Please, try again.'));
        }
        $users = $this->UserProfiles->Users->find('list', ['limit' => 200]);
        $this->set(compact('UserProfile', 'users'));
    }

    public function edit()
    {
        $user_id = $this->Auth->user('id');
        
        $query = $this->UserProfiles->find('all', [
            'conditions' => ['user_id' => $user_id]
        ]);
        $row = $query->first();
        $count = $query->count();
        if($count==0)
        {
            return $this->redirect(['controller'=>'UserProfiles','action' => 'add']);
        }

        $id = $row->id;

        $userProfile = $this->UserProfiles->get($id);

        // debug($userProfile->image); 

        // exit; 

        if ($this->request->is(['patch', 'post', 'put'])) {

            $image_name = $this->request->getData('image-edit.name');
            $image_temp = $this->request->getData('image-edit.tmp_name');

            // debug($userProfile->image);
            // exit;

            if(!empty($image_name))
            {
                $file = new File(WWW_ROOT.'img'.DS.$userProfile->image);
                $file->delete();
                $image_name = Text::uuid().$image_name;
                $destination = WWW_ROOT.'img'.DS.'profile_img'.DS.$image_name;
                move_uploaded_file($image_temp, $destination);
                $this->request->data['image'] = 'profile_img/'.$image_name;
            }

            $userProfile = $this->UserProfiles->patchEntity($userProfile, $this->request->getData());
            $userProfile->user_id = $user_id;
            if ($this->UserProfiles->save($userProfile)) {
                $this->Flash->success(__('The user profile has been updated.'));

                return $this->redirect($this->Auth->logout());
            }
            $this->Flash->error(__('The user profile could not be saved. Please, try again.'));
        }
        $users = $this->UserProfiles->Users->find('list', ['limit' => 200]);
        $this->set(compact('userProfile', 'users'));
    }


    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userProfile = $this->UserProfiles->get($id);
        if ($this->UserProfiles->delete($userProfile)) {
            $this->Flash->success(__('The user profile has been deleted.'));
        } else {
            $this->Flash->error(__('The user profile could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
