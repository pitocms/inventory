<?php
namespace App\Controller;

use App\Controller\AppController;


class UsersController extends AppController
{

    public function login()
    {
        $this->viewBuilder()->setLayout('MaterialLogin');
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error('Your username or password is incorrect.');
        }
    }

    public function logout()
    {
        $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }

    public function changePassword()
    {
        $user = $this->Users->get($this->Auth->user('id'));
        $this->set('user',$user);
        
        if ($this->request->is('post')){
            $user = $this->Users->patchEntity($user,
                [
                    'old_password'     => $this->request->data['old_password'],
                    'password'         => $this->request->data['new_password'],
                    'new_password'     => $this->request->data['new_password'],
                    'confirm_password' => $this->request->data['confirm_password']
                ],
                ['validate' => 'password']
            );

          if ($this->Users->save($user)) {
               $this->Flash->success(__('Password Has Been Changed!!'));
               $this->Auth->logout();
               return $this->redirect(['controller' => 'users','action'=>'login']);
          }
          else {
            $this->Flash->error(__('Password Changed Failed!!Please Try Again.'));
          }
        }
    }

    public function index()
    {
        $this->paginate = [
            'contain' => ['UserTypes']
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['UserTypes','UserProfiles']
        ]);

        $this->set('user', $user);
    }

    public function profileView()
    {
        $id = $this->Auth->user('id');
        $user = $this->Users->get($id, [
            'contain' => ['UserTypes','UserProfiles']
        ]);

        $this->set('user', $user);
    }

    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $userTypes = $this->Users->UserTypes->find('list', ['limit' => 200]);
        $this->set(compact('user', 'userTypes'));
    }

    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['UserProfiles']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $userTypes = $this->Users->UserTypes->find('list', ['limit' => 200]);
        $this->set(compact('user', 'userTypes'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function inactive($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        $user->status = 0;
        if ($this->Users->save($user)) {
            $this->Flash->success(__('The user has been inactive.'));
        } else {
            $this->Flash->error(__('The user could not be inactive. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function active($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        $user->status = 1;
        if ($this->Users->save($user)) {
            $this->Flash->success(__('The user has been Activated.'));
        } else {
            $this->Flash->error(__('The user could not be Activated. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function csv()
    {
        $data = $this->Users->find('all');
        $data = $data;
        $_serialize = 'data';
        $_header = array('id', 'user_type_id','username','password','email','uid','is_email','created', 'modified');

        $this->setResponse($this->getResponse()->withDownload('users.csv'));
        $this->viewBuilder()->setClassName('CsvView.Csv');
        $this->set(compact('data', '_header', '_serialize'));
    }

}
