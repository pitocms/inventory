<?php
namespace App\Controller;

use App\Controller\AppController;

class UserTypesController extends AppController
{

    public function index()
    {
        $userTypes = $this->paginate($this->UserTypes);

        $this->set(compact('userTypes'));
    }

    public function add()
    {
        $userType = $this->UserTypes->newEntity();
        if ($this->request->is('post')) {
            $userType = $this->UserTypes->patchEntity($userType, $this->request->getData());
            if ($this->UserTypes->save($userType)) {
                $this->Flash->success(__('The user type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user type could not be saved. Please, try again.'));
        }
        $this->set(compact('userType'));
    }

    public function edit($id = null)
    {
        $userType = $this->UserTypes->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userType = $this->UserTypes->patchEntity($userType, $this->request->getData());
            if ($this->UserTypes->save($userType)) {
                $this->Flash->success(__('The user type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user type could not be saved. Please, try again.'));
        }
        $this->set(compact('userType'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userType = $this->UserTypes->get($id);
        if ($this->UserTypes->delete($userType)) {
            $this->Flash->success(__('The user type has been deleted.'));
        } else {
            $this->Flash->error(__('The user type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function csv()
    {
        $data = $this->UserTypes->find('all');
        $data = $data;
        $_serialize = 'data';
        $_header = array('id', 'name', 'created', 'modified');

        $this->setResponse($this->getResponse()->withDownload('userTypes.csv'));
        $this->viewBuilder()->setClassName('CsvView.Csv');
        $this->set(compact('data', '_header', '_serialize'));
    }

    public function Search(){

        $this->request->allowMethod('ajax');
        $this->viewBuilder()->layout(false);

        $keyword = $this->request->query('keyword');

        $query = $this->UserTypes->find('all',[
              'conditions' => ['name LIKE'=>'%'.$keyword.'%'],
              'limit' => 10
        ]);

        $this->set('userTypes', $this->paginate($query));
        $this->set('_serialize', ['userTypes']);

    }
}
