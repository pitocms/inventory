<div class="box-header">
    <div class="row">
    <div class="col-md-6">
        <h3 class="box-title heading-before-line"><?= __('Add Profile Information') ?></h3>
    </div>
    </div>
</div>
<br>
<div class="box-body">
    <div class="row">
    <?= $this->Form->create($UserProfile,['type'=>'file']) ?>
    <div class="col-md-12">
        <?php 
            echo $this->BForm->control('image',['type'=>'file','class'=>'image-upload']);
        ?>
    </div>
    <div class="col-md-6">
        <?php
            echo $this->BForm->control('first_name');
            echo $this->BForm->control('last_name');
            echo $this->BForm->control('father_name');
            echo $this->BForm->control('mother_name');
        ?>
    </div>
    <div class="col-md-6">
        <?php
            echo $this->BForm->control('phone_number');
            echo $this->BForm->control('dagination');
            echo $this->Form->control('date_of_birth',['class'=>"datepicker form-control",'label'=>'Date Of Birth','type'=>'text']);
        ?>
    </div>

    <div class="col-md-12">
        <?= $this->Form->button(__('Submit'),['class'=>'btn btn-info btn-margin']) ?>
        <?= $this->Form->end() ?>
    </div>
    </div>
</div>

<script>
    $('document').ready(function(){
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
            , autoclose: true,
            todayHighlight: true
        });
    });
</script>