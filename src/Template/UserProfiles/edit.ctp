<div class="box-header">
    <div class="row">
    <div class="col-md-6">
        <h3 class="box-title heading-before-line"><?= __('Edit Profile') ?></h3>
    </div>
    </div>
</div>
<br>
<div class="box-body">
    <div class="row">
    <?= $this->Form->create($userProfile,['type'=>'file']) ?>

    <div class="col-md-12">

        <span class="cimage">
            <?= empty($userProfile->image) ? $this->Html->image('avatar04.png',['class'=>'image-round blah']):$this->Html->image($userProfile->image,['class'=>'image-round blah']) ?>
        </span>

        <?php 
            echo $this->BForm->control('image-edit',['type'=>'file','class'=>'image-upload','label'=>'Change Profile Picture']);
        ?>
    </div>

    <div class="col-md-6">
        <?php
            echo $this->BForm->control('first_name');
            echo $this->BForm->control('last_name');
            echo $this->BForm->control('father_name');
            echo $this->BForm->control('mother_name');
        ?>
    </div>
    <div class="col-md-6">
        <?php
            echo $this->BForm->control('phone_number');

            echo $this->BForm->control('dagination');

            echo $this->Form->control('date_of_birth',['class'=>"datepicker form-control",'label'=>'Date Of Birth','type'=>'text','value'=>$userProfile->date_of_birth->format('Y-m-d')]);
        ?>
    </div>

    <div class="col-md-12">
        <?= $this->Form->button(__('Submit'),['class'=>'btn btn-info btn-margin']) ?>
        <?= $this->Form->end() ?>
    </div>
    </div>
</div>

<script>

    $(".image-upload").change(function(){
        readURL(this);
    });

    $('document').ready(function(){
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
            , autoclose: true,
            todayHighlight: true
        });
    });
</script>