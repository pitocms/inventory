<div class="box-header">
    <div class="row">
    <div class="col-md-6">
        <h3 class="box-title heading-before-line"><?= __('User Types') ?></h3>
    </div>

    <div class="col-md-6 text-right">
        <?= $this->Html->link('<span class="add-button ion-android-add-circle"></span>',['controller'=>'user-types','action'=>'add'],['escape' => false]) ?>
    </div>
    </div>
</div>
<div class="box-body">
    <hr>
    <div class="search-box">
        <input class="search-button" type="text" name="search" placeholder="Search..">
        <?= $this->Html->link('CSV',['controller'=>'UserTypes','action'=>'csv'],['class'=>'btn btn-info pull-right']) ?>
    </div>

    <div class="table-content">
    <div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">No.</th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userTypes as $no=>$userType): ?>
            <tr>
                <td><?= $this->Number->format($no+1) ?></td>
                <td><?= h($userType->name) ?></td>
                <td><?= $this->Time->nice($userType->created) ?></td>
                <td><?= $this->Time->nice($userType->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('<span class="fa fa-edit"></span>'), ['action' => 'edit', $userType->id],['escape' => false,'class'=>'btn btn-warning btn-xs']) ?>
                    <?= $this->Form->postLink(__('<span class="fa fa-remove"></span>'), ['action' => 'delete', $userType->id], ['class'=>'btn btn-danger btn-xs','escape' => false,'confirm' => __('Are you sure you want to delete # {0}?', $userType->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
    </div>
    </div>
</div>

<script>
    
    $('#Users').addClass('active');
    $('.userTypes').addClass('active');

    $(function() {
       $('.search-button').keyup(function(){
           var keyword = $(this).val();
           searchUserType( keyword );
       })
    });

    function searchUserType( keyword ){
          
          var data = keyword;
          $.ajax(
                {
                    method: 'get',
                    url : "<?php echo $this->Url->build( [ 'controller' => 'UserTypes', 'action' => 'Search' ] ); ?>",
                    data: {keyword:data},

                    success: function( response )
                    {       
                       $( '.table-content' ).html(response);
                    }
                }
          );
    };
</script>