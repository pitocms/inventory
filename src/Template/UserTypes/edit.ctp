<div class="box-header">
    <div class="row">
        <div class="col-md-6">
            <h3 class="box-title heading-before-line"><?= __('Edit User Type') ?></h3>
        </div>

        <div class="col-md-6 text-right">
           <?= $this->Html->link('<span class="add-button ion-ios-list-outline"></span>',['controller'=>'user-types','action'=>'index'],['escape' => false]) ?>
        </div>

    </div>
</div>

<div class="box-body">
<?= $this->Form->create($userType) ?>
<div class="row">
    <div class="col-md-6">
    <?php
        echo $this->BForm->control('name',['label'=>'Type Name :']);
    ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $this->Form->button(__('Submit'),['class'=>'btn btn-info btn-margin']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

</div>

<script>
    $('#Users').addClass('active');
    $('.userTypes').addClass('active');
</script>