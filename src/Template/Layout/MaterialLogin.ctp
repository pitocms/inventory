<?php
$cakeDescription = 'CakePHP: adminLte';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <?= $this->Html->css(['custome']) ?>

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <section class="content">
        <div class="row">
            <div class="col m4 offset-m4" style="padding:25px;margin-top: 6%;">
                <?= $this->fetch('content') ?>
            </div>
        </div>
        <div class="row">
            <div class="col m4 offset-m4" style="padding-left:35px;">
               <?= $this->Flash->render() ?>
            </div>
        </div>
    </section>

    <footer class="page-footer" id="footer">
        <div class="container" style="text-align: right;">
          © 2018 Copyright P I T O C M S</p>
        </div>
    </footer>
</body>
</html>
