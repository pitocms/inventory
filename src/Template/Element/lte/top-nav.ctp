<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
  <!-- Sidebar toggle button-->
  <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
    <span class="sr-only">Toggle navigation</span>
  </a>
  <!-- Navbar Right Menu -->
  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
      <!-- User Account: style can be found in dropdown.less -->
      <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <?php 
              $image = 'avatar5.png';
              if(!empty($userAuth['user_profile']['image'])):
                  $image = $userAuth['user_profile']['image'];
              endif; 
              echo $this->Html->image($image,['class'=>'user-image']);  
          ?>
          <span class="hidden-xs text-capitalize"><?= $userAuth['username']; ?></span>
        </a>
        <ul class="dropdown-menu">
          <!-- User image -->
          <li class="user-header">
            <?php 
              $image = 'avatar5.png';
              if(!empty($userAuth['user_profile']['image'])):
                  $image = $userAuth['user_profile']['image'];
              endif; 
              echo $this->Html->image($image,['class'=>'img-circle']); 
            ?>

            <p>
            <span class="text-capitalize">
              <?= @$userAuth['user_profile']['first_name']." ".@$userAuth['user_profile']['last_name'] ?> - <?= @$userAuth['user_profile']['dagination'] ?>
            </span>
            <small>
              Member since 
              <?php  
                if(!empty($userAuth['created'])):
                  $now = $userAuth['created'];
                  echo $now->timeAgoInWords(
                     ['format' => 'MMM d, YYY', 'end' => '+1 year']
                  );
                endif;
              ?>    
            </small>
            </p>
          </li>
          <!-- Menu Footer-->
          <li class="user-footer">
            <div class="pull-left">
              <?= $this->Html->link("Profile",['controller'=>'Users','action'=>'profileView'],['class'=>'btn btn-default btn-flat'])  ?>
            </div>
            <div class="pull-right">
              <?php 
                  echo $this->Html->link('Sign out',['controller'=>'Users','action'=>'logout'],['class'=>'btn btn-default btn-flat','escape' => false]
                  ); 
              ?>
            </div>
          </li>
        </ul>
      </li>
      <!-- Control Sidebar Toggle Button -->
      <!-- <li>
        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
      </li> -->
    </ul>
  </div>

</nav>

