<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <?php 
            $image = 'avatar5.png';
              if(!empty($userAuth['user_profile']['image'])):
                  $image = $userAuth['user_profile']['image'];
              endif; 
              echo $this->Html->image($image,['class'=>'img-circle']);
          ?>
        </div>
        <div class="pull-left info">
          <p class="text-capitalize"><?= $userAuth['username']; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <p class="text-right" style="margin-right: 15px;"><?= $this->Html->link('Edit Profile',['controller'=>'userProfiles','action'=>'edit']); ?></p>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

        <li class="treeview" id="Users">
          <a href="#">
            <i class="fa fa-users"></i> <span>Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="user_list"><a href="<?= $this->Url->build('/users') ?>"><i class="fa fa-circle-o"></i> User List</a></li>
            <li class="userTypes"><a href="<?= $this->Url->build('/userTypes') ?>"><i class="fa fa-circle-o"></i> User Types</a></li>
            <li class="changePassword"><a href="<?= $this->Url->build('/users/changePassword') ?>"><i class="fa fa-circle-o"></i> Change Password</a></li>
          </ul>
        </li>
        <!-- <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>