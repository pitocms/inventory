<div class="box-header">
    <div class="row">
      <div class="col-md-6">
          <h3 class="box-title heading-before-line"><?= __('Change Password') ?></h3>
      </div>
    </div>
    <hr>
</div>


<div class="box-body">
<div class="row">
     <?= $this->Form->create($user,['type'=>'post']) ?>
   
        <div class="col-md-6">
          <?= $this->Form->input('old_password',array('class'=>'form-control','required'=>true,'type'=>'password','label'=>'Current Password')) ?>
          <?= $this->Form->input('new_password',array('class'=>'form-control','required'=>true,'type'=>'password')) ?>
          <?= $this->Form->input('confirm_password',array('class'=>'form-control','required'=>true,'type'=>'password')) ?>
        </div>

        <div class="col-md-12">
            <?= $this->Form->button(__('Submit'),['class'=>'btn btn-info btn-margin']) ?>
            <?= $this->Form->end() ?>
        </div>

</div>  <!-- user-form-end -->
</div>
</div>

<script>
    $('#Users').addClass('active');
    $('.changePassword').addClass('active');
</script>



