<div class="box-header">
    <div class="row">
    <div class="col-md-6">
        <h3 class="box-title heading-before-line"><?= __('Add User') ?></h3>
    </div>

    <div class="col-md-6 text-right">
        <?= $this->Html->link('<span class="add-button ion-ios-list-outline"></span>',['controller'=>'Users','action'=>'index'],['escape' => false]) ?>
    </div>
    </div>
</div>

<div class="box-body">
    <div class="row">
    <?= $this->Form->create($user) ?>
        <div class="col-md-6">
        <?php
            echo $this->BForm->control('user_type_id', ['options' => $userTypes]);
            echo $this->BForm->control('username');
            echo $this->BForm->control('email');
        ?>
        </div>
        <div class="col-md-6">
        <?php
            echo $this->BForm->control('password');
            echo $this->BForm->control('confirm_password',array('type'=>'password'));
        ?>
        </div>
        <div class="col-md-12">
            <?= $this->Form->button(__('Submit'),['class'=>'btn btn-info btn-margin']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<script>
    $('#Users').addClass('active');
    $('.user_list').addClass('active');
</script>