<div class="box-header">
    <div class="row">
    <div class="col-md-6">
        <h3 class="box-title heading-before-line"><?= __('Users') ?></h3>
    </div>

    <div class="col-md-6 text-right">
        <?= $this->Html->link('<span class="add-button ion-android-add-circle"></span>',['controller'=>'users','action'=>'add'],['escape' => false]) ?>
    </div>
    </div>
</div>

<div class="box-body">
    <hr>
    <div class="search-box">
        <input class="search-button" type="text" name="search" placeholder="Search..">
        <?= $this->Html->link('CSV',['controller'=>'Users','action'=>'csv'],['class'=>'btn btn-info pull-right']) ?>
    </div>

    <div class="table-content">
    <div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">No.</th>
                <th scope="col"><?= $this->Paginator->sort('user_type_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('username') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>

                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $no=>$user): ?>
            <tr>
                <td><?= $this->Number->format($no+1) ?></td>
                <td><?= $user->has('user_type') ? $this->Html->link($user->user_type->name, ['controller' => 'UserTypes', 'action' => 'view', $user->user_type->id]) : '' ?></td>
                <td><?= h($user->username) ?></td>
                <td><?= h($user->email) ?></td>
                <td><?= $this->Time->nice($user->created) ?></td>
                <td><?= $this->Time->nice($user->modified) ?></td>
            
                <td class="actions">
                    <?= $this->Html->link(__('<span class="fa fa-eye"></span>'), ['action' => 'view', $user->id],['escape' => false,'class'=>'btn btn-info btn-xs']) ?>

                    <?= $this->Html->link(__('<span class="fa fa-edit"></span>'), ['action' => 'edit', $user->id],['escape' => false,'class'=>'btn btn-warning btn-xs']) ?>

                    <?= $this->Form->postLink(__('<span class="fa fa-remove"></span>'), ['action' => 'delete', $user->id], ['class'=>'btn btn-danger btn-xs','escape' => false,'confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
                    
                    <?php if($user->status==1): ?>
                    <?= $this->Form->postLink(__('Inactive'), ['action' => 'inactive', $user->id], ['class'=>'btn btn-danger btn-xs','confirm' => __('Are you sure you want to inactive this user?')]) ?>
                    <?php endif; ?>

                    <?php if($user->status==0): ?>
                    <?= $this->Form->postLink(__('Active'), ['action' => 'active', $user->id], ['class'=>'btn btn-success btn-xs','confirm' => __('Are you sure you want to active this user?')]) ?>
                    <?php endif; ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
    </div>
    </div>
</div>

<script>
    $('#Users').addClass('active');
    $('.user_list').addClass('active');
</script>