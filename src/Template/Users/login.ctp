<div class="col">
  <h2 class="l-m-heading">Complain Management System</h2>
  <h3 class="heading-before-line mat-login-heading">Log in</h3>
</div>
<?= $this->Form->create() ?>
<?php 
$myTemplates = [
    'inputContainer' => '<div class="input-field col s12 {{type}}{{required}}">{{content}}</div>'
];
$this->Form->templates($myTemplates);
?>

<?= $this->Form->control('email',['required' => true]) ?>

<?= $this->Form->control('password',['required' => true]) ?>
<div class="col">
<?= $this->Form->button('Login',['class'=>'waves-effect waves-light btn']) ?>

<p><a href="">Forgot Password?</a></p>
</div>
<?= $this->Form->end() ?>