<div class="box-body">

    <?php 
        //debug($user);
    ?>
    <div class="row">
    <div class="user-info text-center">
        <div class="col-md-6 col-md-offset-3">
            <?=
                empty($user->user_profile->image) ? $this->Html->image('avatar5.png',['class'=>'image-profile']):$this->Html->image($user->user_profile->image,['class'=>'image-profile'])
            ?>
            <p class="text-capitalize">
                Name : <?= empty($user->user_profile->first_name)?"User profile not update.":$user->user_profile->first_name." ".$user->user_profile->last_name ?>
                <br>
                Email : <?= h($user->email) ?>
                <br>
                Phone : <?= empty($user->user_profile->phone_number)?"User profile not update.":$user->user_profile->phone_number ?>
                    
            </p>
        </div>
        
    </div>
    </div>
    
    <div class="row">
    <div class="col-md-6 col-md-offset-3 text-center">
        <table class="vertical-table table text-center table-striped table-bordered">
            <tr>
                <th scope="row"><?= __('User Type') ?></th>
                <td><?= $user->has('user_type') ? $this->Html->link($user->user_type->name, ['controller' => 'UserTypes', 'action' => 'view', $user->user_type->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Username') ?></th>
                <td><?= h($user->username) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Email') ?></th>
                <td><?= h($user->email) ?></td>
            </tr>

            <tr>
                <th scope="row"><?= __('Father Name') ?></th>
                <td><?= empty($user->user_profile->father_name)?"User profile not update.":$user->user_profile->father_name ?></td>
            </tr>

            <tr>
                <th scope="row"><?= __('Mother Name') ?></th>
                <td><?= empty($user->user_profile->mother_name)?"User profile not update.":$user->user_profile->mother_name ?></td>
            </tr>

            <tr>
                <th scope="row"><?= __('Date Of Birth') ?></th>
                <td>
                    <?= empty($user->user_profile->date_of_birth)?"User profile not update.":$this->Time->timeAgoInWords($user->user_profile->date_of_birth,['format' => 'MMM d, YYY', 'end' => '+1 year']) ?>
                </td>
            </tr>

            <tr>
                <th scope="row"><?= __('Profile Created') ?></th>
                <td><?= $this->Time->timeAgoInWords($user->created,['format' => 'MMM d, YYY', 'end' => '+1 year']) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Profile Modified') ?></th>
                <td><?= $this->Time->timeAgoInWords($user->modified,['format' => 'MMM d, YYY', 'end' => '+1 year']) ?></td>
            </tr>
        </table>
    </div>
    </div>
</div>

<script>
    $('#Users').addClass('active');
    $('.user_list').addClass('active');
</script>