<?php
namespace App\Test\TestCase\View\Helper;

use App\View\Helper\BFormHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\BFormHelper Test Case
 */
class BFormHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\View\Helper\BFormHelper
     */
    public $BForm;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->BForm = new BFormHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BForm);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
