<?php
namespace App\Test\TestCase\View\Helper;

use App\View\Helper\ControlFieldHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\ControlFieldHelper Test Case
 */
class ControlFieldHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\View\Helper\ControlFieldHelper
     */
    public $ControlField;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->ControlField = new ControlFieldHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ControlField);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
