<?php
use Migrations\AbstractSeed;

/**
 * Users seed.
 */
class UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'user_type_id' => '1',
                'username' => 'pitocms',
                'password' => '$2y$10$yNkGHkgp4GPVFTLhnTgyGennLZ/AFOCL7ZnOLIORpjaQ5EdfrtWeC',
                'email' => 'pitocms@yahoo.com',
                'uid' => '',
                'is_email' => '0',
                'status' => '1',
                'created' => '2020-07-19 07:51:03',
                'modified' => '2020-07-19 07:57:57',
            ],
        ];

        $table = $this->table('users');
        $table->insert($data)->save();
    }
}
